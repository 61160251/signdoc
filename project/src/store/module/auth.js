import {AUTH_LOGIN,AUTH_LOGOUT} from '../mutation-types'
import router from '../../router'
import api from '../../services/api'
export default {
    namespaced: true,
    state: () =>({
        user: null,
    }),
    mutations: {
        [AUTH_LOGIN](state, payload){
          state.user = payload
        },
        [AUTH_LOGOUT] (state){
          state.user = null
        }
      },
      actions: {
       async login ({commit}, payload){
            console.log(payload)
            try{
            const res = await axios.post('/auth/login',{username: payload.email, password: payload.password })
            const user = res.data.user
            const token = res.data.token
            localStorage.setItem('token', token)
            localStorage.setItem('user', user)
            console.log(res)
          commit(AUTH_LOGIN, user)
            } catch (e) {
            console.log(error)
          }
        },
        logout ({commit}){
           commit(AUTH_LOGOUT)
        }
      },
      getters: { 
          isLogin (state, getters){
            return state.user != null
          }
      }
}