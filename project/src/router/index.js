import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import PopupSuccess from '../components/PopupSuccess.vue'
// import Login from '../views/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/popupLoad',
    name: 'PopupLoad',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupLoad.vue')
  },
  {
    path: '/popupCancel',
    name: 'PopupCancel',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupCancel.vue')
  },
  {
    path: '/teacher',
    name: 'Teacher',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Teacher.vue')
  },
  {
    path: '/popupDelecte',
    name: 'PopupDelecte',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupDelecte.vue')
  },
  {
    path: '/popupCancel',
    name: 'PopupCancel',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupCancel.vue')
  },
  {
    path: '/popupSend',
    name: 'PopupSend',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../components/PopupSend.vue')
  },
  {
    path: '/popupCancelTeacher',
    name: 'PopupCancelTeacher',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../componentTeacher/PopupCancelTeacher.vue')
  },
  {
    path: '/officer',
    name: 'Officer',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Officer.vue')
  },
  {
    path: '/student',
    name: 'Student',
    // component: Popup
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ '../views/Student.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
