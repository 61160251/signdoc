const express = require('express')
const router = express.Router()
const Document = require('../models/Document')
const SendingDetail = require('../models/SendingDetail')

const documents = [
    // {
    //     id: 1,
    //     user_id: '005',
    //     filename: '61160251_RE01',
    //     date: '03/12/2564',
    //     status: 'examine',
    //     type: 'RE01',
    // },
    // {
    //     id: 2,
    //     user_id: '006',
    //     filename: '61160251_RE02',
    //     date: '05/12/2564',
    //     status: 'examine',
    //     type: 'RE02',
    // }
]
let lastId = 3

const getDocuments = async function (req, res, next) {
    try {
        const documents = await Document.find({}).then()
        res.json(documents)
    } catch (err) {
        return res.status(500).send({
            message: err.message
        })
    }
}

const getDocument = async function (req, res, next) {
    const id = req.params.id
    try {
        const document = await Document.find({userId:id}).exec()
        res.json(document)
        if (document == null) {
            return res.status(404).json({
                message: 'Document not found'
            })
        }
    } catch (err) {
        res.status(404).json({
            message: err.message
        })
    }

}

// const getDocumentbyUser_id = function(req,res,next){
//     const index = documents.findIndex(function(item){
//         return item.user_id == req.params.user_id
//     })
//     res.json(documents[index])
// }

const addDocuments = async function (req, res, next) {
    const newDocument = new Document({
        userId: req.body.userId,
        filename: req.body.filename,
        date: req.body.date,
        status: req.body.status,
        type: req.body.type,
    })
    try {
        await newDocument.save()
        res.status(201).json(newDocument)
    } catch (err) {
        return res.status(201).send({
            message: err.message
        })
    }
}

const updateDocument = async function (req, res, next) {
    const documentId = req.params.id
    try {
        const sendingDetail = await SendingDetail.findOne({_id: sendingDetail[i].stuId})
        const document = await Document.findById(documentId)
        document.userId = req.body.userId
        document.filename = req.body.filename
        document.date = req.body.date
        document.status = req.body.status
        document.type = req.body.type
        await document.save()
        return res.status(200).json(document)
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}

const deleteDocument = async function (req, res, next) {
    const docId = req.params.id
    const sendingDetailId = await SendingDetail.find({docId:docId}).exec()
    try {
        await Document.findByIdAndDelete(docId)
        await SendingDetail.findByIdAndDelete(sendingDetailId)
        return res.status(200).send()
    } catch (err) {
        return res.status(404).send({ message: err.message })
    }
}



router.get('/', getDocuments)
router.get('/:id', getDocument)
// router.get('/:user_id', getDocumentbyUser_id)
router.post('/', addDocuments)
router.put('/:id', updateDocument)
router.delete('/:id', deleteDocument)

module.exports = router