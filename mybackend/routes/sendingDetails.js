const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const SendingDetail = require('../models/SendingDetail')
const Document = require('../models/Document')
const User = require('../models/User')

const sendingDetails = [
    // {
    //     id:'001',
    //     doc_id:'001',
    //     stu_id:'005',
    //     tec_id:'002',
    //     note:'can you sign this file pls'
    // },
    // {
    //     id:'002',
    //     doc_id:'002',
    //     stu_id:'005',
    //     tec_id:'003',
    //     note:'can you sign this file pls'
    // }
]
const getSendingDetails = async function (req,res,next){
    try{
        const sendingDetails = await SendingDetail.find({}).then()
        res.json(sendingDetails)
      } catch(err){
        return res.status(500).send({ 
          message: err.message
        })
      }
}

const getSendingDetail = async function(req,res,next){
    const id = req.params.id
    try{
        const sendingDetail = await SendingDetail.find({$or:[{stuId:id},{tecId:id}]}).exec()
        var sendingList = []
        for (i = 0; i < sendingDetail.length; i++){
            var detail = {}
            const document = await Document.findOne({_id: sendingDetail[i].docId})
            const student = await User.findOne({_id: sendingDetail[i].stuId})
            detail.name = student.name
            detail.studentId = student.studentId
            detail.docId = sendingDetail[i].docId
            detail.fileName = document.fileName
            detail.docType = document.type
            detail.note = sendingDetail[i].note

            sendingList.push(detail)
            console.log(sendingDetail)
        }
        res.json(sendingList)
        if (sendingList==null){
            return res.status(404).json({
                message: 'SendingDetail not found'
            })
        }
    }catch (err){ 
        res.status(404).json({
            message: err.message
        })
    }
    
}
const addSendingDetails = async function (req,res,next){
    const newSendingDetail = new SendingDetail({
        docId: req.body.docId,
        stuId: req.body.stuId,
        tecId: req.body.tecId,
        note: req.body.note,
    })
    try {
        await newSendingDetail.save()
        res.status(201).json(newSendingDetail)  
    }catch (err){
        return res.status(201).send({
          message: err.message
        })
    }
}

const updateSendingDetail = async function (req,res,next){
    const sendingDetailId = req.params.id
    try{
        const sendingDetail = await SendingDetail.findById(sendingDetailId)
        sendingDetail.docId= req.body.docId,
        sendingDetail.stuId= req.body.stuId,
        sendingDetail.tecId= req.body.tecId,
        sendingDetail.note= req.body.note,
        await sendingDetail.save()
        return res.status(200).json(sendingDetail)
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

const deleteSendingDetail = async function (req,res,next){
    const sendingDetailId = req.params.id
    try{
        await SendingDetail.findOneAndDelete({docId: sendingDetailId})
        return res.status(200).send()
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

router.get('/', getSendingDetails)
router.get('/:id', getSendingDetail)
router.post('/', addSendingDetails)
router.put('/:id', updateSendingDetail)
router.delete('/:id', deleteSendingDetail)

  
module.exports = router