const express = require('express')
const { reset } = require('nodemon')
const router = express.Router()
const Position = require('../models/Position')

const positions =[ 
    // {
    //     name:'อธิการบดี',
    //     users:'001'
    // },
    // {
    //     name:'อาจารย์ที่ปรึกษาวิทยาการคอมพิวเตอร์ ชั้นที่ 1',
    //     users:['002','003']
    // }
]

const getPositions = async function (req,res,next){
    try{
        const positions = await Position.find({}).then()
        res.json(positions)
      } catch(err){
        return res.status(500).send({
          message: err.message
        })
      }
}
const getPosition = async function(req,res,next){
    const namePos = req.params.namePos
    try{
        const position = await Position.find(namePos).exec()
        res.json(position)
        if (position==null){
            return res.status(404).json({
                message: 'Position not found'
            })
        }
    }catch (err){
        res.status(404).json({
            message: err.message
        })
    }
    
}
const addPositions = async function (req,res,next){
    const newPosition = new Position({
        namePos: req.body.namePos,
        users: req.body.users
    })
    try {
        await newPosition.save()
        res.status(201).json(newPosition)  
    }catch (err){
        return res.status(201).send({
          message: err.message
        })
    }
}

const updatePosition = async function (req,res,next){
    const positionId = req.params.id
    try{
        const position = await Position.findById(positionId)
        position.namePos = req.body.namePos
        position.users = req.body.users
        await position.save()
        return res.status(200).json(position)
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

const deletePosition = async function (req,res,next){
    const positionId = req.params.id
    try{
        await Position.findByIdAndDelete(positionId)
        return res.status(200).send()
    }catch (err){
        return res.status(404).send({message: err.message})
    }
}

router.get('/', getPositions)
router.get('/:name', getPosition)
router.post('/', addPositions)
router.put('/:id', updatePosition)
router.delete('/:id', deletePosition)
  
module.exports = router