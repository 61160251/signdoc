const { ROLE } = require('../constant.js')
const mongoose = require('mongoose')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/database')
async function clearUser(){
    await User.deleteMany({})
}
async function main (){
    await clearUser()

    const user1 = new User({email: '61160036@go.buu.ac.th',password: 'palapol123',roles:[ROLE.STUDENT],name: 'พลพล คุ้มไข่น้ำ',type: 'student',branch:'CS',studentId:'61160036',teacherId:''})
    user1.save()
    const user2 = new User({email: 'arabic@go.buu.ac.th',password: 'arabic123',roles:[ROLE.TEACHER],name: 'arabic number',type: 'teacher',branch:'CS',studentId:'',teacherId:''})
    user2.save()
    const user3 = new User({email: 'indigo@go.buu.ac.th',password: 'arabic123',roles:[ROLE.OFFICER],name: 'indigo color',type: 'officer',branch:'CS',studentId:'',teacherId:''})
    user3.save()

    
}

main().then(function(){
    console.log('Finish')
})