const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const mongoose = require('mongoose')
const { authenMiddleware } = require('./helpers/auth')

const indexRouter = require('./routes/index')
const documentsRouter = require('./routes/documents') 
const usersRouter = require('./routes/users')
const positionsRouter = require('./routes/positions')
const sendingDetailsRouter = require('./routes/sendingDetails')
const authRouter = require('./routes/auth')
const dotenv = require('dotenv');

dotenv.config()
mongoose.connect('mongodb://localhost:27017/database')

const app = express()
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/documents', documentsRouter)
// app.use('/users', authenMiddleware,usersRouter)
app.use('/users', usersRouter)
app.use('/positions', positionsRouter)
app.use('/sendingDetails', sendingDetailsRouter)
app.use('/auth', authRouter)

module.exports = app
