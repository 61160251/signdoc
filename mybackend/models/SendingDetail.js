const mongoose = require('mongoose')
const { Schema } = mongoose
const sendingDetailSchema = Schema({
    docId: String,
    stuId: String,
    tecId: String,
    note: String

})

module.exports = mongoose.model('SendingDetail',sendingDetailSchema)