const mongoose = require('mongoose')
const { Schema } = mongoose
const documentSchema = Schema({
    userId: String,
    fileName: String,
    date: Date,
    status:String,
    type:String,

})

module.exports = mongoose.model('Document',documentSchema)