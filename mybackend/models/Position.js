const mongoose = require('mongoose')
const { Schema } = mongoose
const positionSchema = Schema({
    namePos: String,
    users: [String],

})

module.exports = mongoose.model('Position',positionSchema)